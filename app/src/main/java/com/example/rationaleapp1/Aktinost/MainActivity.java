package com.example.rationaleapp1.Aktinost;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import android.widget.Toast;

import com.example.rationaleapp1.R;
import com.example.rationaleapp1.model.Album;
import com.example.rationaleapp1.net.ApiPoint;
import com.example.rationaleapp1.net.ApiService;

import java.util.List;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity  {


    public static String ALBUM_KEY = "ALBUM_KEY";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        ApiPoint service = ApiService.getRetrofitInstance().create(ApiPoint.class);
        Call<List<Album>> call = service.getAllAlbums();

        call.enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                

                List<Album> albums = response.body();
                final ListView listView = findViewById(R.id.list);
                ListAdapter adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1,albums);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Album a=(Album)listView.getItemAtPosition(position);
                        Intent intent=new Intent(MainActivity.this, ImageAct.class);
                        intent.putExtra(ALBUM_KEY,a.getId());
                        startActivity(intent);
                            //INTENT
                    }
                });

            }

            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Neuspelo dobavljanje albuma", Toast.LENGTH_SHORT).show();;
            }
        });
    }

}



