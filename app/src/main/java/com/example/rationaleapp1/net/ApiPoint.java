package com.example.rationaleapp1.net;

import com.example.rationaleapp1.model.Album;
import com.example.rationaleapp1.model.Photo;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiPoint {

    @GET("/albums")
    Call<List<Album>>getAllAlbums();

    //call za listu albuma

     @GET("/albums/{id}/photos")
    Call<List<Photo>> getAllPhotos(@Path("id") int albumId);
    //poziv za slike



}
