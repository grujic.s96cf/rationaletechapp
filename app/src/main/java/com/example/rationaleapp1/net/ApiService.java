package com.example.rationaleapp1.net;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    static OkHttpClient okHttpClient = new OkHttpClient();

    public static Retrofit getRetrofitInstance(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Contract.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }


    public static ApiPoint apiInterface(){
        ApiPoint apiService = getRetrofitInstance().create(ApiPoint.class);
        return apiService;
    }


}
