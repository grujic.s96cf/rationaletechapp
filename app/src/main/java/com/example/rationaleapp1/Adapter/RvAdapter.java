package com.example.rationaleapp1.Adapter;






import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.rationaleapp1.R;
import com.example.rationaleapp1.model.Photo;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RvAdapter extends RecyclerView.Adapter<RvAdapter.MyViewHolder> {

    private List<Photo> listaSlika;
    private ItemClickListener listener;


    public RvAdapter(List<Photo> listaSlika, ItemClickListener listener) {
        this.listaSlika = listaSlika;
        this.listener=listener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textTitle;
        ImageView image;
        TextView textUrl;


        public MyViewHolder(View itemView) {
            super(itemView);

            textTitle =itemView. findViewById(R.id.title_photo);
            image = itemView.findViewById(R.id.image_photo);
            textUrl = itemView.findViewById(R.id.url_photo);


        }

        public void bind(final Photo photo, final ItemClickListener listener) {
            textTitle.setText(photo.getTitle());
            textUrl.setText(photo.getUrl());
            Picasso.get().load(photo.getThumbnailUrl()).into(image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 if(listener!=null){

                         listener.onItemClicked(photo);
                     }

                 }

            });
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(listaSlika.get(position),listener);


    }

    @Override
    public int getItemCount() {
        return listaSlika.size();
    }
    public interface ItemClickListener {
        void onItemClicked(Photo photo);
    }

}








