package com.example.rationaleapp1.Aktinost;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;
import com.example.rationaleapp1.Adapter.RvAdapter;
import com.example.rationaleapp1.R;
import com.example.rationaleapp1.model.Photo;
import com.example.rationaleapp1.net.ApiPoint;
import com.example.rationaleapp1.net.ApiService;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ImageAct extends AppCompatActivity implements RvAdapter.ItemClickListener {

    private RvAdapter myAdapter;
    private RecyclerView myRecyclerView;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rv_slika);
        myRecyclerView = findViewById(R.id.myRecyclerView);


       int key= getIntent().getExtras().getInt(MainActivity.ALBUM_KEY);


        ApiPoint service = ApiService.getRetrofitInstance().create(ApiPoint.class);
        Call<List<Photo>> call = service.getAllPhotos(key);

        call.enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                final List<Photo> listaSlika = response.body();


                myRecyclerView = findViewById(R.id.myRecyclerView);
                myRecyclerView.setLayoutManager(new LinearLayoutManager(ImageAct.this));
                myAdapter = new RvAdapter(listaSlika,ImageAct.this);
                myRecyclerView.setAdapter(myAdapter);

            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {
                Toast.makeText(ImageAct.this, "Unable to load picture", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onItemClicked(Photo photo) {
        Intent intent=new Intent(ImageAct.this, ImageViewAct.class);
        intent.putExtra(ImageViewAct.WEBSITE_ADDRESS, photo.getUrl());
        startActivity(intent);
    }
}


